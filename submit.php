<?php
		if (isset($_POST['submit'])) 
		{ 
			ob_start();
    	$conn = new PDO('mysql:host=devcsrms.db.11380501.fee.hostedresource.net;dbname=devcsrms;charset=utf8', 'devcsrms', 'HDvjgdkty6@%');
    	$conn -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		//applicant information
			$first_name = ucwords($_POST['fname']);
			$middle_name =  ucwords($_POST['mname']);
			$last_name =  ucwords($_POST['lname']);
			$birth_date =  ucwords($_POST['birthdate']);

		//address information

			$streetAddress1 =  ucwords($_POST['streetAddress1']);
			$streetAddress2 =  ucwords($_POST['streetAddress2']);
			$postZip = $_POST['postZip'];
			$province = $_POST['province'];
			$municipality = $_POST['municipality'];

		//employment desired 
			$positionChosen = $_POST['positionChosen'];
			$startDate = $_POST['startDate'];
			$desiredSalary = $_POST['desiredSalary'];
			$workedBefore = $_POST['workedBefore'];
			$appliedBefore = $_POST['appliedBefore'];

		//Educational Background
			$highSchool =  ucwords($_POST['highSchool']);
			$hsYearsAttended = $_POST['hsYearsAttended'];
			$graduatedHS = $_POST['graduatedHS'];
			$college =  ucwords($_POST['college']);
			$areaDegreeCollege =  ucwords($_POST['areaDegreeCollege']);
			$CollegeYearsAttended = $_POST['CollegeYearsAttended'];
			$graduatedCollege = $_POST['graduatedCollege'];
			$otherSchools =  ucwords($_POST['otherSchools']);
			$areaDegreeOthers = $_POST['areaDegreeOthers'];
			$numOfYearsAttended = $_POST['numOfYearsAttended'];
			$graduatedOthers = $_POST['graduatedOthers'];

		//skills qualifications
			$skills = $_POST['skills'];
			$qualification = $_POST['qualifications'];

		//current employment
			$currentEmployer =  ucwords($_POST['currentEmployer']);
			$positionCurrent =  ucwords($_POST['positionCurrent']);
			$currentSalary = $_POST['currentSalary'];
			$reasonLeavingCurrent = $_POST['reasonLeavingCurrent'];
			$startDateCurrent = $_POST['startDateCurrent'];
			$contactCurrent = $_POST['contactCurrent'];
			$currentSupervisor =  ucwords($_POST['currentSupervisor']);
			$contactSupervisor = $_POST['contactSupervisor'];
			$emailSupervisor = $_POST['emailSupervisor'];

		//previous employment
			$previousEmployer =  $_POST['previousEmployer'];
			$positionPrevious =  $_POST['positionPrevious'];
			$salaryPrevious = $_POST['salaryPrevious'];
			$reasonLeavingPrevious = $_POST['reasonLeavingPrevious'];
			$startDatePrevious = $_POST['startDatePrevious'];
			$contactPrevious = $_POST['contactPrevious'];
			$endDatePrevious = $_POST['endDatePrevious'];
			$previousSupervisor =  $_POST['previousSupervisor'];
			$contactPrevSupervisor = $_POST['contactPrevSupervisor'];
			$emailPrevSupervisor = $_POST['emailPrevSupervisor'];


		//references 1-3
			$reference =  $_POST['reference'];
			$relationship =  $_POST['relationship'];
			$yearsAquainted = $_POST['yearsAquainted'];
			$contactReference = $_POST['contactReference'];
			$emailReference = $_POST['emailReference'];

			//resume
			$file = rand(1000,100000)."-".$_FILES['resume']['name'];
			$file_loc = $_FILES['resume']['tmp_name'];
			$file_size = $_FILES['resume']['size'];
			$file_type = $_FILES['resume']['type'];
			$folderResume="resume/";

		    //coverletter
			$file_cover_letter = rand(1000,100000)."-".$_FILES['coverLetter']['name'];
			$file_loc_cover = $_FILES['coverLetter']['tmp_name'];
			$file_size_cover = $_FILES['coverLetter']['size'];
			$file_type_cover = $_FILES['coverLetter']['type'];
			$folderCoverLetter = "coverLetter/";

			//personal contact information
			$email = $_POST['email'];
			$areaCode = $_POST['areaCode'];
			$contactNumber = $_POST['phoneNumber'];
			$mobileNumber = $_POST['mobileNumber'];

			 date_default_timezone_set('Asia/Manila');
             $date = date("Y-m-d H:i:s");
             $status = 0;
             $application_status = "Pending";


			move_uploaded_file($file_loc,$folderResume.$file);
			move_uploaded_file($file_loc_cover,$folderCoverLetter.$file_cover_letter);

			//if no other attended schools
			if (empty($otherSchools)) {
				$otherSchools = "None";

			}
			if (empty($areaDegreeOthers)) {
				$areaDegreeOthers = "None";
			}
			if (empty($numOfYearsAttended)) {
				$numOfYearsAttended = 0;
				$graduatedOthers = "No";
			}

			//if no telephone
			if (empty($areaCode) || empty($contactNumber) ) {
				$areaCode = 0;
				$contactNumber = "None";
			}
			
			
		//Submission time and date 
			$submission_info = $conn->prepare("INSERT INTO submission_info(fk_employee_id,submission_date,submission_status,application_status) VALUES (:fk_employee_id,:sub_date,:stat,:app_stat)");


		//query for applicant info form
			$ApplicantInfo = $conn->prepare("INSERT INTO employee_information(first_name,middle_name,last_name,birth_date) VALUES (:firstname,:middlename,:lastname,:birthdate)");

		//query for addresses form
			$AddressInfo = $conn->prepare("INSERT INTO personal_information(fk_employee_id,street_address,street_address2,state_province,municipality,zip_code) VALUES (:fk_id,:st1,:st2,:state_prov,:muni,:zip_code)");

		// query for contact details form
			$contactDetails = $conn->prepare("INSERT INTO contact_details(fk_employee_id,email,phone_area_code,phone_number,mobile_number) VALUES (:fk_id,:emailAdd,:area_code,:contact_number,:mobile)");

		// Employment Desired form 
			$employmentDesired = $conn->prepare("INSERT INTO employment_desired(fk_employee_id,position,start_date,desired_salary,worked_before,applied_before) VALUES (:fk_id,:position_chosen,:start_date,:desired_salary,:worked_before,:applied_before)");


		//Educational Background form
			$educationalBg = $conn->prepare("INSERT INTO educational_background(fk_employee_id,high_school,	highschool_years_attended,highschool_graduate,college,area_degree,college_years_attended,college_graduate,other_Schools,area_degree_others,years_attended_others,graduated_others) VALUES (:fk_id,:hsName,:hsYears,:hsGrad,:collegeName,:areaDegree,:collegeYears,:collegeGrad,:other_Schools,:area_DegreeOthers,:numYears_Others,:graduated_Others)");

		//skills qualification form
			$skillsQualification = $conn ->prepare("INSERT INTO skills_qualifications(fk_employee_id,skills,qualifications) VALUES (:fk_id,:skillSet,:qualificationSet)");

			$currentEmployment = $conn ->prepare("INSERT INTO current_employment(fk_employee_id,current_employer,position,salary,reason_leaving,start_date,contact,supervisor,contact_number,email) VALUES (:fk_id,:current_employer,:position_current,:salary_current,:current_reason,:current_start_date,:contact_current,:current_supervisor,:contact_supervisor,:email_supervisor)");

			$previousEmployment = $conn ->prepare("INSERT INTO previous_employment(fk_employee_id,previous_employer,position,salary,reason_leaving,start_date,end_date,supervisor,contact_number,email_address,contact)VALUES (:fk_id,:prev_employer,:position_previous,:salary_previous,:prev_reason,:start_date_prev,:end_date_prev,:prev_supervisor,:contact_prev_supervisor,:email_prev_supervisor,:contact_prev)");

			$references = $conn ->prepare("INSERT INTO reference(fk_employee_id,reference_person,relationship,years_aquainted,contact_reference,email_reference)VALUES (:fk_id,:reference_name,:reference_relationship,:years_aquainted,:contact_reference,:email_reference)");

			$resumeUpload = $conn ->prepare("INSERT INTO applicant_resume(fk_employee_id,resume_path,type)VALUES (:fk_id,:resumePath,:resumeType)");

			$coverLetterUpload = $conn ->prepare("INSERT INTO cover_letter(fk_employee_id,cover_letter_path,type)VALUES (:fk_id,:coverPath,:coverType)");

			$ApplicantInfo ->execute(array(
				':firstname' => $first_name,
				':middlename' => $middle_name, 	
				':lastname' => $last_name,
				':birthdate' => $birth_date
			));


		//last id from inserted query
			$id = $conn->lastInsertId(); 


		 //Check if there is a previous employer
			$scanForm = array('previousEmployer', 'positionPrevious', 'salaryPrevious', 'reasonLeavingPrevious', 'startDatePrevious', 'contactPrevious','endDatePrevious','previousSupervisor','contactPrevSupervisor','emailPrevSupervisor');
		//check if there is a current employment	
			$scanForm2 = array('currentEmployer', 'positionCurrent', 'currentSalary', 'reasonLeavingCurrent', 'startDateCurrent','contactCurrent','currentSupervisor','contactSupervisor','emailSupervisor');

			//check if there is a current employment	
			$scanForm2 = array('currentEmployer', 'positionCurrent', 'currentSalary', 'reasonLeavingCurrent', 'startDateCurrent','contactCurrent','currentSupervisor','contactSupervisor','emailSupervisor');



			if (!empty($file_loc)) {
			$resumeUpload ->execute(array(
				':fk_id' => $id,
				':resumePath' => $file, 	
				':resumeType' => $file_type
			));
		}

			if (!empty($file_loc_cover)) {
			$coverLetterUpload ->execute(array(
				':fk_id' => $id,
				':coverPath' => $file_cover_letter, 	
				':coverType' => $file_type_cover
			));
}


			$error2 = false;

			foreach($scanForm2 as $scanForm2) {
				if (empty($_POST[$scanForm2])) {
					$error2 = true;
				}
			}



			$AddressInfo ->execute(array(
				':fk_id' => $id,
				':st1' => $streetAddress1,
				':st2' => $streetAddress2,
				':state_prov' => $province,
				':zip_code' => $postZip,
				':muni' => $municipality
			));


			$contactDetails ->execute(array(
				':fk_id' => $id,
				':emailAdd' => $email,
				':area_code' => $areaCode,
				':contact_number' => $contactNumber,
				':mobile' => $mobileNumber
			));


			$employmentDesired ->execute(array(
				':fk_id' => $id,
				':position_chosen' => $positionChosen,
				':start_date' => $startDate,
				':desired_salary' => $desiredSalary,
				':worked_before' => $workedBefore,
				':applied_before' => $appliedBefore

			));


			$educationalBg ->execute(array(
				':fk_id' => $id,
				':hsName' => $highSchool,
				':hsYears' => $hsYearsAttended,
				':hsGrad' => $graduatedHS,
				':collegeName' => $college,
				':areaDegree' => $areaDegreeCollege,
				':collegeYears' => $CollegeYearsAttended,
				':collegeGrad' => $graduatedCollege,
				':other_Schools' => $otherSchools,
				':area_DegreeOthers' => $areaDegreeOthers,
				':numYears_Others' => $numOfYearsAttended,
				':graduated_Others' => $graduatedOthers
			));


			$skillsQualification ->execute(array(
				':fk_id' => $id,
				':skillSet' => $skills,
				':qualificationSet' => $qualification
			));

			$submission_info ->execute(array(
				':fk_employee_id' => $id,
				':sub_date' => $date,
				':stat' => $status,
				':app_stat' => $application_status

			));

			if ($error2) {
	
			} else {

			$currentEmployment ->execute(array(
				':fk_id' => $id,
				':current_employer' => $currentEmployer,
				':position_current' => $positionCurrent,
				':salary_current' => $currentSalary,
				':current_reason' => $reasonLeavingCurrent,
				':current_start_date' => $startDateCurrent,
				':contact_current' => $contactCurrent,
				':current_supervisor' => $currentSupervisor,
				':contact_supervisor' => $contactSupervisor,
				':email_supervisor' => $emailSupervisor
			));
		}



				foreach( $previousEmployer as $key => $n ) {
		 //added this condition to avoid extra null column insertion in table
					if($n !=''){ 	

						$previousEmployment ->execute(array(
							':fk_id' => $id,
							':prev_employer' => ucwords($previousEmployer[$key]),
							':position_previous' =>  ucwords($positionPrevious[$key]),
							':salary_previous' => $salaryPrevious[$key],
							':start_date_prev' => $startDatePrevious[$key],
							':prev_reason' => $reasonLeavingPrevious[$key],
							':end_date_prev' => $endDatePrevious[$key],
							':prev_supervisor' =>  ucwords($previousSupervisor[$key]),
							':contact_prev_supervisor' => $contactPrevSupervisor[$key],
							':email_prev_supervisor' => $emailPrevSupervisor[$key],
							':contact_prev' => $contactPrevious[$key]
						));
					}

				}

			

			foreach( $reference as $count => $k ) {
		 //added this condition to avoid extra null column insertion in table
				if($k !=''){ 	

					$references ->execute(array(
						':fk_id' => $id,
						':reference_name' => ucwords($reference[$count]),
						':reference_relationship' => ucwords($relationship[$count]),
						':years_aquainted' => $yearsAquainted[$count],
						':contact_reference' => $contactReference[$count],
						':email_reference' => $emailReference[$count]

					));


				}
			}	
			 exit(header("location:http://dev.commsoft-rms.com/applicationform?success=1"));
		}
		ob_end_flush();
		?>
