  <?php

  include "config.php";

  ?>

  <!DOCTYPE html>
  <html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Job Application Form</title>
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="assets/bootstrap/css/bootstrap.min.css">
   
  </head>

  <body onload="myFunction(), prov()">

     <script>
function myFunction() {
   $('#exampleModal').modal('show') 
}
</script>




    <div class="container">


      <div class="col-sm-12 col-md-4 col-lg-4">
        <img src="assets/logo/logo.png" alt="CommSoft-RMS" class="img-responsive">
      </div>

      <hr>

      <nav aria-label="breadcrumb">
        <ol class="breadcrumb" style="background-color: #B8B7B7!important;">
          <li class="breadcrumb-item active" aria-current="page"><h5 class="margin-0">Employment Application:</h5></li>
        </ol>
      </nav>
      <hr>

      <nav aria-label="breadcrumb">
        <ol class="breadcrumb" style="width:auto; background-color: #F5F5F5;">
          <li class="breadcrumb-item active" aria-current="page">Personal Information</li>
        </ol>
      </nav>


      <!--Employee Information -->
      <form action="submit.php" method="POST" enctype="multipart/form-data">
        <div class="form-row col-md-12">
          <div class="col-sm-12 col-md-4 col-lg-4">
            <label>First name</label>
            <input type="text" class="form-control" name="fname" required="">
          </div>
          <div class="col">
            <label>Middle Name</label>
            <input type="text" class="form-control" name="mname" required="">
          </div>
          <div class="col">
            <label>Last Name</label>
            <input type="text" class="form-control" name="lname" required="" >
          </div>
        </div>


        <div class="form-row col-md-12">
          <div class="col-sm-12 col-md-4 col-lg-4">
            <label>Birth Date</label>
            <input type="date" class="form-control" name="birthdate" required="">
          </div>
        </div>


        <!-- Address/location-->
        <hr>
        <nav aria-label="breadcrumb">
          <ol class="breadcrumb" style="width:auto; background-color: #F5F5F5;">
            <li class="breadcrumb-item active" aria-current="page">Address</li>
          </ol>
        </nav>

        <div class="form-group">
          <div class="col-sm-12 col-md-12 col-lg-12">
            <label for="">Street Address</label>
            <input type="text" class="form-control" name="streetAddress1" required="">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-12 col-md-12 col-lg-12">
            <label>Street Address Line 2</label>
            <input type="text" class="form-control" name="streetAddress2"  required="">
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-12 col-md-12 col-lg-12">
            <label for="">Postal/Zip Code</label>
            <input type="number" class="form-control" name="postZip" required="">
          </div>
        </div>


        <!-- Drop Down for provinces Form, javascript below page -->

        <div class="form-row col-md-12">
          <div class="col-sm-12 col-md-4 col-lg-4">
            <label for="inputState">Province</label>
            <select id="province" class="form-control" name="province" required>
              <option value=""></option>
              <option value="Abra">Abra</option>
              <option value="Agusan del Norte">Agusan del Norte</option>
              <option value="Agusan del Sur">Agusan del Sur</option>
              <option value="Aklan">Aklan</option>
              <option value="Albay">Albay</option>
              <option value="Antique">Antique</option>
              <option value="Apayao">Apayao</option>
              <option value="Aurora">Aurora</option>
              <option value="Basilan">Basilan</option>
              <option value="Bataan">Bataan</option>
              <option value="Batanes">Batanes</option>
              <option value="Batangas">Batangas</option>
              <option value="Benguet">Benguet</option>
              <option value="Biliran">Biliran</option>
              <option value="Bohol">Bohol</option>
              <option value="Bukidnon">Bukidnon</option>
              <option value="Bulacan">Bulacan</option>
              <option value="Cagayan">Cagayan</option>
              <option value="Camarines Norte">Camarines Norte</option>
              <option value="Camarines Sur">Camarines Sur</option>
              <option value="Camiguin">Camiguin</option>
              <option value="Capiz">Capiz</option>
              <option value="Catanduanes">Catanduanes</option>
              <option value="Cavite">Cavite</option>
              <option value="Cebu">Cebu</option>
              <option value="Compostela Valley">Compostela Valley</option>
              <option value="Cotabato">Cotabato</option>
              <option value="Davao del Norte">Davao del Norte</option>
              <option value="Davao del Sur">Davao del Sur</option>
              <option value="Davao Oriental">Davao Oriental</option>
              <option value="Dinagat Islands">Dinagat Islands</option>
              <option value="Eastern Sama">Eastern Samar</option>
              <option value="Guimaras">Guimaras</option>
              <option value="Ifugao">Ifugao</option>
              <option value="Ilocos Norte">Ilocos Norte</option>
              <option value="Ilocos Sur">Ilocos Sur</option>
              <option value="Iloilo">Iloilo</option>
              <option value="Isabela">Isabela</option>
              <option value="Kalinga">Kalinga</option>
              <option value="La Union">La Union</option>
              <option value="Laguna">Laguna</option>
              <option value="Lanao del Norte">Lanao del Norte</option>
              <option value="Lanao del Sur">Lanao del Sur</option>
              <option value="Leyte">Leyte</option>
              <option value="Maguindanao">Maguindanao</option>
              <option value="Marinduque">Marinduque</option>
              <option value="Masbate">Masbate</option>
              <option value="Metro Manila">Metro Manila</option>
              <option value="Misamis Occidental">Misamis Occidental</option>
              <option value="Misamis Oriental">Misamis Oriental</option>
              <option value="Mountain Province">Mountain Province</option>
              <option value="Negros Occidental">Negros Occidental</option>
              <option value="Negros Oriental">Negros Oriental</option>
              <option value="Northern Samar">Northern Samar</option>
              <option value="Nueva Ecija">Nueva Ecija</option>
              <option value="Nueva Vizcaya">Nueva Vizcaya</option>
              <option value="Nueva Vizcaya">Nueva Vizcaya</option>
              <option value="Occidental Mindoro">Occidental Mindoro</option>
              <option value="Oriental Mindoro">Oriental Mindoro</option>
              <option value="Palawan">Palawan</option>
              <option value="Rizal">Rizal</option>
              <option value="Zamboanga Sibugay">Zamboanga Sibugay</option>
            </select>
          </div>

          <!-- Drop Down for provinces Form -->
          <div class="col">
            <label for="inputState">City/Municipality</label>
            <select id="city" class="form-control" name="municipality" required>
                <option value=""></option>
                <option value="Bangued">Bangued</option>
                <option value="Boliney"Boliney</option>
                <option value="Bucay">Bucay</option>
                <option value="Bucloc">Bucloc</option>
                <option value="Daguioman">Daguioman</option>
                <option value="Danglas">Danglas</option>
                <option value="Dolores">Dolores</option>
                <option value="La Paz">La Paz</option>
                <option value="Lacub">Lacub</option>
                <option value="Lagangilang">Lagangilang</option>
                <option value="Lagayan">Lagayan</option>
                <option value="Langiden">Langiden</option>
                <option value="Licuan-Baay">Licuan-Baay</option>
                <option value="Luba">Luba</option>
                <option value="Malibcong">Malibcong</option>
                <option value="Manabo">Manabo</option>
                <option value="Peñarrubia">Peñarrubia</option>
                <option value="Pidigan">Pidigan</option>
                <option value="Pilar">Pilar</option>
                <option value="Sallapadan">Sallapadan</option>
                <option value=">San Isidro">San Isidro</option>
                <option value="San Juan">San Juan</option>
                <option value="San Quintin">San Quintin</option>
                <option value="Tayum">Tayum</option>
                <option value="Tineg">Tineg</option>
                <option value="Tubo">Tubo</option>
                <option value="Villaviciosa">Villaviciosa</option>
              </select>
            </div>
          </div>
          <hr>

          <!-- Contact Information -->
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="width:auto; background-color: #F5F5F5;">
              <li class="breadcrumb-item active" aria-current="page">Contact Information</li>
            </ol>
          </nav>

          <div class="form-group">
            <div class="col-sm-12 col-md-12 col-lg-12">
              <label for="">Email Address</label>
              <input type="name" class="form-control" name="email" required="">
            </div>
          </div>

          <div class="form-row col-md-12">
            <div class="col-sm-12 col-md-4 col-lg-2">
              <label for="inputEmail4">Area Code</label>
              <input type="number" class="form-control" name="areaCode" >
            </div>
            <div class="col-sm-12 col-md-4 col-lg-5">
              <label for="inputEmail4">Telephone Number (optional)</label>
              <input type="number" class="form-control"  onkeydown="javascript: return event.keyCode == 69 ? false : true" name="phoneNumber">
            </div>
            <div class="col-sm-12 col-md-4 col-lg-5">
              <label for="inputEmail4">Mobile Number</label>
              <input type="number" class="form-control"  onkeydown="javascript: return event.keyCode == 69 ? false : true" name="mobileNumber" required="">
            </div>
          </div>

          <hr>


          <!-- Employemnt Desired Form -->
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="width:auto; background-color: #F5F5F5;">
              <li class="breadcrumb-item active" aria-current="page">Employment Desired</li>
            </ol>
          </nav>

          <div class="form-row col-md-12">
            <div class=" col col-sm-12 col-md-12 col-lg-12">
              <label>Position Applying For</label>
              <select id="" class="form-control" name="positionChosen" required="">
                <option></option>
                <option value="Technical Support Representative">Technical Support Representative</option>
                <option value="Call Center Representative">Call Center Representative</option>
                <option value="Customer Care Representative">Customer Care Representative</option>
                <option value="Accounting Officer">Accounting Officer</option>
                <option value="Admin Associate">Admin Associate</option>
                <option value="Sales Representative">Sales Representative</option>
                <option value="Sales Executive">Sales Executive</option>
                <option value="Sales Manager">Sales Manager</option>
              </select>
            </div>

            <div class="col-sm-12 col-md-4 col-lg-4">
              <label>Date You Can Start</label>
              <input type="date" class="form-control" name="startDate" required="">
            </div>

            <div class="col-sm-12 col-md-4 col-lg-4">
              <label>Salary Desired</label>
              <input type = "text" class="form-control money numberinput" name="desiredSalary" required="">
            </div>
          </div>


          <div class="form-row col-sm-12 col-md-12 col-lg-12 ">
            <div class="col col-sm-6 col-md-5 col-lg-3">       
              <label>Have You Worked Here Before?</label>
              <div class="btn-group" data-toggle="buttons">
                <label class="btn active " style="background-color: #353535!important">
                  <input type="radio"  name="workedBefore" value="yes"  autocomplete="off" checked  required=""><a style="color: white;">Yes</a>
                </label>
                <label class="btn btn-primary" style="background-color: #870B23!important">
                  <input type="radio" name="workedBefore" value="No"  autocomplete="off"> No
                </label>
              </div>
            </div>

            <div class="col col-sm-6 col-md-5 col-lg-3">       
              <label>Have You Applied Here Before?</label>
              <div class="btn-group" data-toggle="buttons">
                <label class="btn active " style="background-color: #353535!important">
                  <input type="radio"  name="appliedBefore" value="yes"  autocomplete="off" checked  required=""><a style="color: white;">Yes</a>
                </label>
                <label class="btn btn-primary" style="background-color: #870B23!important">
                  <input type="radio" name="appliedBefore" value="No"  autocomplete="off"> No
                </label>
              </div>
            </div>

          </div>

          <hr>


          <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="width:auto; background-color: #F5F5F5;">
              <li class="breadcrumb-item active" aria-current="page">Education</li>
            </ol>
          </nav>


          <hr>
          <!-- High School Form -->
          <div class="form-row col-sm-12 col-md-12 col-lg-12">
            <div class="col-sm-12 col-md-6 col-lg-5">
              <label>High School</label>
              <input type="text" class="form-control" placeholder="Name of High School Attended" name="highSchool" required="">
            </div>

            <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-5">
              <label >Number Of Years Attended</label>
              <select id="city" class="form-control" name="hsYearsAttended"  required="">
                <option value="0">0</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
              </select>
            </div>




           <div class=" col-2 col-sm-2 col-md-2 col-lg-2 col-xl-2">       
              <label>Graduated</label>
              <div class="btn-group" data-toggle="buttons">
                <label class="btn active " style="background-color: #353535!important">
                  <input type="radio"  name="graduatedHS" value="yes"  autocomplete="off" checked><a style="color: white;" required="">Yes</a>
                </label>
                <label class="btn btn-primary" style="background-color: #870B23!important">
                  <input type="radio" name="graduatedHS" value="No"  autocomplete="off"> No
                </label>
              </div>
            </div>

          </div>

          <hr>

          <!-- College Form -->
            <div class="col-sm-12 col-md-12 col-lg-12"> 
              <label for="">College</label>
              <input type="text" class="form-control" id=""  placeholder="Name of College/University Attended" name="college" >
            </div>

            <div class="form-row col-12 col-sm-12 col-md-12 col-lg-12">
               <div class=" col-12 col-sm-12 col-md-12 col-lg-5 col-xl-6">  
                <label for="inputEmail4">Area of Study/Degree</label>
                <input type="text" class="form-control" name="areaDegreeCollege" required="">
              </div>

              <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <label >Number Of Years Attended</label>
                <select id="city" class="form-control" name="CollegeYearsAttended"  required="">
                  <option value="0">0</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                </select>
              </div>

              <div class="col-2 col-sm-2 col-md-3 col-lg-2 col-xl-2">       
                <label>Graduated</label>
                <div class="btn-group" data-toggle="buttons">
                  <label class="btn active " style="background-color: #353535!important">
                    <input type="radio"  name="graduatedCollege" value="yes"  autocomplete="off" checked  required=""><a style="color: white;">Yes</a>
                  </label>
                  <label class="btn btn-primary" style="background-color: #870B23!important">
                    <input type="radio" name="graduatedCollege" value="No"  autocomplete="off"> No
                  </label>
                </div>
              </div>
          </div>

          <hr>

          <!-- trade/other Form -->
          <div class="form-group">
            <div class="col-sm-12 col-md-5 col-lg-12">
              <label for="">Trade School/Other</label>
              <input type="text" class="form-control" id="" placeholder="Name of Trade/Technical/Other School Attended" name="otherSchools">
            </div>

             <div class="form-row col-12 col-sm-12 col-md-12 col-lg-12">
               <div class=" col-12 col-sm-12 col-md-12 col-lg-5 col-xl-6"> 
                <label for="inputEmail4">Area of Study/Degree</label>
                <input type="text" class="form-control" name="areaDegreeOthers" >
              </div>

               <div class="col-12 col-sm-12 col-md-4 col-lg-4 col-xl-4">
                <label >Number Of Years Attended</label>
                <select id="city" class="form-control" name="numOfYearsAttended" >
                  <option value="0">0</option>
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                  <option value="6">6</option>
                  <option value="7">7</option>
                  <option value="8">8</option>
                  <option value="9">9</option>
                  <option value="10">10</option>
                </select>
              </div>

               <div class="col-2 col-sm-2 col-md-3 col-lg-2 col-xl-2">        
                <label>Graduated</label>
                <div class="btn-group" data-toggle="buttons">
                  <label class="btn active " style="background-color: #353535!important;">
                    <input type="radio"  name="graduatedOthers" value="yes"  autocomplete="off" checked ><a style="color: white;">Yes</a>
                  </label>
                  <label class="btn btn-primary" style="background-color: #870B23!important;">
                    <input type="radio" name="graduatedOthers" value="No"  autocomplete="off"> No
                  </label>
                </div>
              </div>
            </div>
          </div>

          <hr>

          <!-- Skills/Qualifications-->
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="width:auto; background-color: #F5F5F5;">
              <li class="breadcrumb-item active" aria-current="page">Skills/Qualifications</li>
            </ol>
          </nav>


          <div class="col-sm-12 col-md-12 col-lg-12">
            <label for="inputEmail4">Skills (List any Relevant Skills)</label><br>
            <textarea class = "col-sm-12 col-md-12 col-lg-12 rounded form-control" name="skills"  required=""></textarea>
          </div>


          <div class="col-sm-12 col-md-12 col-lg-12">
            <label for="inputEmail4">Qualifications (List any relevant Certifications or Qualifications)</label><br>
            <textarea class = "col-sm-12 col-md-12 col-lg-12 rounded form-control" name="qualifications" required=""></textarea>
          </div>

          <hr>

          <!-- Current Employment Form -->
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="width:auto; background-color: #F5F5F5;">
              <li class="breadcrumb-item active" aria-current="page">Current Employment</li>
            </ol>
          </nav>

          <div class="form-row col-md-12">
            <div class="col-sm-12 col-md-4 col-lg-4">
              <label for="inputEmail4">Current Employer</label>
              <input type="text" class="form-control" placeholder="Name of Current Employer" name="currentEmployer">
            </div>
            <div class="col">
              <label >Position</label>
              <input type="text" class="form-control" name="positionCurrent" placeholder = "Current Position" >
            </div>
          </div>

          <div class="form-row col-md-12">
            <div class="col-sm-12 col-md-4 col-lg-4">
              <label >Salary</label>
              <input type = "text" class="form-control money numberinput" name="currentSalary">
            </div>
            <div class="col">
              <label for="inputEmail4">Reason For Leaving</label>
              <input type="text" class="form-control" name="reasonLeavingCurrent" >
            </div>
          </div>

          <div class="form-row col-12 col-sm-12 col-md-12 col-lg-12">
            <div class="col-sm-12 col-md-4 col-lg-4">
              <label>Start Date</label>
              <input type="date" class="form-control" name="startDateCurrent">
            </div>

            <div class="col-5 col-sm-5 col-md-3 col-lg-2 col-xl-2">      
                <label>May We Contact?</label>
                <div class="btn-group" data-toggle="buttons">
                  <label class="btn active " style="background-color: #353535!important;">
                      <input type="radio"  name="contactCurrent" value="yes"  autocomplete="off" checked ><a style="color: white;">Yes</a>
                    </label>
                  <label class="btn btn-primary" style="background-color: #870B23!important;">
                    <input type="radio" name="contactCurrent" value="No"  autocomplete="off"> No
                  </label>
                </div>
            </div>
          </div>

          <div class="form-row col-md-12">
            <div class="col-12 col-sm-12 col-md-4 col-lg-4">
              <label for="inputEmail4">Supervisor</label>
              <input type="text" class="form-control" name="currentSupervisor">
            </div>
            <div class="col">
              <label for="inputEmail4">Contact Number</label>
              <input type="number" class="form-control" onkeydown="javascript: return event.keyCode == 69 ? false : true" name="contactSupervisor" >
            </div>
            <div class="col">
              <label for="inputEmail4">Email Address</label>
              <input type="email" class="form-control" name="emailSupervisor" >
            </div>
          </div>

          <hr>
         
          <div class="form-row">  
                  <table class="table table-bordered col-12 col-sm-12 col-md-12 col-lg-12"" id="dynamic_field" >  
                           <tr>  
                              <td>

                               <div class="form-row col-md-12">
              <div class="col-sm-12 col-md-4 col-lg-4">
                <label for="inputEmail4">Previous Employer</label>
                <input type="text" class="form-control" placeholder="Name of Previous Employer" name="previousEmployer[]">
              </div>
              <div class="col">
                <label>Position</label>
                <input type="text" class="form-control" name="positionPrevious[]" placeholder = "Previous Position">
              </div>
            </div>

            <div class="form-row col-md-12">
              <div class="col-sm-12 col-md-4 col-lg-4">
                <label for="inputEmail4">Salary</label>
                <input type = "text"  class="form-control money numberinput" name="salaryPrevious[]" >
              </div>
              <div class="col">
                <label for="inputEmail4">Reason For Leaving</label>
                <input type="text" class="form-control" name="reasonLeavingPrevious[]">
              </div>
            </div>

            <div class="form-row col-md-12">

              <div class="col-sm-12 col-md-4 col-lg-4">
                <label>Start Date</label>
                <input type="date" class="form-control" name="startDatePrevious[]">
              </div>

              <div class="col-sm-12 col-md-4 col-lg-4">
                <label>End Date</label>
                <input type="date" class="form-control" name="endDatePrevious[]">
              </div>

              <div class="col-sm-12 col-md-2 col-lg-2"> 
                <label>May We Contact?</label>
                <select id="" class="form-control" name="contactPrevious[]">
                  <option value="yes">Yes</option>
                  <option value="no">No</option>
                </select>
              </div>
            </div>

            <div class="form-row col-md-12">
              <div class="col-sm-12 col-md-4 col-lg-4">
                <label for="inputEmail4">Supervisor</label>
                <input type="text" class="form-control" name="previousSupervisor[]">
              </div>
              <div class="col">
                <label for="inputEmail4">Contact Number</label>
                <input type="number" class="form-control" name="contactPrevSupervisor[]" onkeydown="javascript: return event.keyCode == 69 ? false : true">
              </div>
              <div class="col">
                <label for="inputEmail4">Email Address</label>
                <input type="email" class="form-control" name="emailPrevSupervisor[]">
              </div>
              <hr>
            </div>
          </td>    
      </tr>  
    </table> 
    <div class="form-row col-sm-12 col-md-12 col-lg-12 mb-3">
       <input type="button" name="add" id="add" class="btn btn-primary form-control" value="Add Previous Employments History" style= "background-color: #353535!important; border:none; color:white; word-wrap: break-word!important;">
     </div> 
    </div>


          <!-- References -->
          <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="width:auto; background-color: #F5F5F5;">
              <li class="breadcrumb-item active" aria-current="page">References</li>
            </ol>
          </nav>

          <div class="form-row col-md-12">
            <div class="col-sm-12 col-md-4 col-lg-4">
              <label for="inputEmail4">Reference 1</label>
              <input type="text" class="form-control" name="reference[]" required="" >
            </div>
            <div class="col">
              <label for="inputEmail4">Relationship</label>
              <input type="text" class="form-control" name="relationship[]"  required="">
            </div>
            <div class="col">
              <label for="inputEmail4">Years Acquainted</label>
              <input type="number" class="form-control" name="yearsAquainted[]"  required="">
            </div>
          </div>

          <div class="form-row col-md-12">
            <div class="col-sm-12 col-md-4 col-lg-4">
              <label for="inputEmail4">Phone</label>
              <input type="number" class="form-control"  onkeydown="javascript: return event.keyCode == 69 ? false : true" name="contactReference[]"  required="">
            </div>
            <div class="col">
              <label for="inputEmail4">Email</label>
              <input type="email" class="form-control" name="emailReference[]" required="">
            </div>
          </div>
          <hr>

          <div class="form-row col-md-12">
            <div class="col-sm-12 col-md-4 col-lg-4">
              <label for="inputEmail4">Reference 2</label>
              <input type="text" class="form-control" name="reference[]"  required="">
            </div>
            <div class="col">
              <label for="inputEmail4">Relationship</label>
              <input type="text" class="form-control" name="relationship[]" required="">
            </div>
            <div class="col">
              <label for="inputEmail4">Years Acquainted</label>
              <input type="number" class="form-control" name="yearsAquainted[]"  required="">
            </div>
          </div>

          <div class="form-row col-md-12">
            <div class="col-sm-12 col-md-4 col-lg-4">
              <label for="inputEmail4">Phone</label>
              <input type="number" class="form-control"  onkeydown="javascript: return event.keyCode == 69 ? false : true" name="contactReference[]"  required="">
            </div>
            <div class="col">
              <label for="inputEmail4">Email</label>
              <input type="email" class="form-control" name="emailReference[]"  required="" >
            </div>
          </div>

          <hr>

          <div class="form-row col-md-12">
            <div class="col-sm-12 col-md-4 col-lg-4">
              <label for="inputEmail4">Reference 3</label>
              <input type="text" class="form-control" name="reference[]" required="">
            </div>
            <div class="col">
              <label for="inputEmail4">Relationship</label>
              <input type="text" class="form-control"  name="relationship[]"  required="">
            </div>
            <div class="col">
              <label for="inputEmail4">Years Acquainted</label>
              <input type="number" class="form-control" name="yearsAquainted[]"  required="">
            </div>
          </div>

          <div class="form-row col-md-12">
            <div class="col-sm-12 col-md-4 col-lg-4">
              <label for="inputEmail4">Phone</label>
              <input type="number" class="form-control"  onkeydown="javascript: return event.keyCode == 69 ? false : true" name="contactReference[]"  required="">
            </div>
            <div class="col">
              <label for="inputEmail4">Email</label>
              <input type="email" class="form-control" name="emailReference[]"  required="">
            </div>
          </div>

          <hr>

          <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="width:auto; background-color: #F5F5F5;">
              <li class="breadcrumb-item active" aria-current="page">Cover Letter & Resume (Optional): </li>
            </ol>
          </nav>

          <div class="form-row col-md-12">

            <div class="col-sm-12 col-md-4 col-lg-4 mt-1">
              <label>Resume</label>
              <input class="btn btn-secondary form-control" type="file" name="resume"  multiple="multiple" />
            </div>

            <div class="col-sm-12 col-md-4 col-lg-4 mt-1">
              <label>Cover Letter</label>
              <input class="btn btn-secondary form-control" type="file" name="coverLetter" multiple="multiple" />
            </div>

            <hr>
          </div>

          <nav aria-label="breadcrumb">
            <ol class="breadcrumb" style="width:auto; background-color: #F5F5F5; ">
              <li class="breadcrumb-item active text-justify" aria-current="page">Send Application</li>
            </ol>
          </nav>

          <p class="font-weight-light" style="text-align: justify;">By clicking the submit button below, I certify that all of the information provided by me on this application is true and complete, and I understand that if any false information, ommissions, or misrepresentations are discovered, my application may be rejected and, if I am employed, my employement may be terminated at any time.<br>  

            In consideration of my employment, I agree to conform to the company's rules and regulations, and I agree that my employment and compenstation can be terminated, with or without cause, and with or without notice, at any time, at either my or the company's option.<br>  

          I also understand and agree that the terms and conditions of my employment may be changed, with or without cause, and with or without notice, at any time by the company.</p>


          <hr>
          <div class="col-12 col-sm-12 col-md-12 col-lg-12">
            <button type="submit" name = "submit" class="btn btn-primary form-control" style="background-color: #40A23D!important; border:none; ">Submit</button>
          </div>

          <!--success modal-->
          <?php if($_GET['success'] == 1){ ?>
          <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body text-center">
              <h5>Thank You For Your Interest In Joining Our Company</h5><br>
              <h6>Please keep your lines open as we review your application.</h6>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <a href="http://commsoft-rms.com"><button type="button" class="btn btn-primary">Back to site</button></a>
              </div>
            </div>
          </div>
        </div>

             
          <?php } ?>
     
      </form>
      <footer class="col-lg-12 text-center mt-3">Contact Us: Support@commsoft-rms.com | (02)5432741 | 09985754709 </footer>

      </div>

    <script src="assets/js/polyfiller.js"></script>
    <script src="assets/js/jquery-3.2.1.slim.min.js"></script>
    <script src="assets/js/simple.money.format.js"></script>
    <script src="assets/js/city.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script> 
    <script src="assets/bootstrap/js/popper.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>

    



      <script>  

  function prov(){  
    
  var $ = new City();
  $.showProvinces("#province");
  $.showCities("#city");
  // ------------------
  // additional methods 
  // -------------------
  // will return all provinces 
  console.log($.getProvinces());

  // will return all cities 
  console.log($.getAllCities());

  // will return all cities under specific province (e.g Batangas)
  console.log($.getCities("Batangas")); 
}


 $(document).ready(function () {
             $(".numberinput").forceNumeric();
         });


         // forceNumeric() plug-in implementation
         jQuery.fn.forceNumeric = function () {

             return this.each(function () {
                 $(this).keydown(function (e) {
                     var key = e.which || e.keyCode;

                     if (!e.shiftKey && !e.altKey && !e.ctrlKey &&
                     // numbers   
                         key >= 48 && key <= 57 ||
                     // Numeric keypad
                         key >= 96 && key <= 105 ||
                     // comma, period and minus, . on keypad
                        key == 190 || key == 188 || key == 109 || key == 110 ||
                     // Backspace and Tab and Enter
                        key == 8 || key == 9 || key == 13 ||
                     // Home and End
                        key == 35 || key == 36 ||
                     // left and right arrows
                        key == 37 || key == 39 ||
                     // Del and Ins
                        key == 46 || key == 45)
                         return true;

                     return false;
                 });
             });
         }


        $(document).ready(function(){  
          var i=1;  
          $('#add').click(function(){  
           i++;  
           $('#dynamic_field').append('<tr id="row'+i+'"><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove float-sm-right rounded" style = "background-color:#840404">Remove</button><div class="form-row col-md-12"><div class="col-sm-12 col-md-4 col-lg-4"> <label for="inputEmail4">Previous Employer</label><input type="text" class="form-control" placeholder="Name of Previous Employer" name="previousEmployer[]"></div><div class="col"><label>Position</label><input type="text" class="form-control" name="positionPrevious[]" placeholder = "Previous Position" ></div></div><div class="form-row col-md-12"><div class="col-sm-12 col-md-4 col-lg-4"><label for="inputEmail4">Salary</label><input type = "text"  class="form-control money numberinput" name="salaryPrevious[]" ></div><div class="col"><label for="inputEmail4">Reason For Leaving</label><input type="text" class="form-control" name="reasonLeavingPrevious[]"></div></div><div class="form-row col-md-12"><div class="col-sm-12 col-md-4 col-lg-4"><label>Start Date</label><input type="date" class="form-control" name="startDatePrevious[]"></div><div class="col-sm-12 col-md-4 col-lg-4"><label>End Date</label><input type="date" class="form-control" name="endDatePrevious[]"></div><div class="col-sm-12 col-md-2 col-lg-2"> <label>May We Contact?</label><select id="" class="form-control" name="contactPrevious[]"><option value="yes">Yes</option><option value="no">No</option></select></div></div><div class="form-row col-md-12"><div class="col-sm-12 col-md-4 col-lg-4"><label for="inputEmail4">Supervisor</label><input type="text" class="form-control" name="previousSupervisor[]"></div><div class="col"><label for="inputEmail4">Contact Number</label><input type="number" class="form-control" name="contactPrevSupervisor[]" onkeydown="javascript: return event.keyCode == 69 ? false : true"></div><div class="col"><label for="inputEmail4">Email Address</label> <input type="email" class="form-control" name="emailPrevSupervisor[]"></div></div></div></td></tr>');  
              });  

           $(document).on('click', '.btn_remove', function(){  
             var button_id = $(this).attr("id");   
             $('#row'+button_id+'').remove();  
        });  

   }); 

        $(document).on('click','#add', function() {
        	 var input =  $('.money').simpleMoneyFormat();
		});


  function FilterInput(event) {
    var keyCode = ('which' in event) ? event.which : event.keyCode;

    isNotWanted = (keyCode == 69 || keyCode == 101);
    return !isNotWanted;
  };


  function handlePaste (e) {
    var clipboardData, pastedData;

  // Get pasted data via clipboard API
  clipboardData = e.clipboardData || window.clipboardData;
  pastedData = clipboardData.getData('Text').toUpperCase();

  if(pastedData.indexOf('E')>-1) {
  //alert('found an E');
  e.stopPropagation();
  e.preventDefault();
  }
  };

    $('.money').simpleMoneyFormat();


  
  </script>
  </body>
  </html>